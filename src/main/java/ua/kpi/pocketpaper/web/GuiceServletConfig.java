package ua.kpi.pocketpaper.web;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public class GuiceServletConfig extends com.google.inject.servlet.GuiceServletContextListener {

    private Module[] guiceModules;

    public GuiceServletConfig(Module... guiceModules) {
        this.guiceModules = guiceModules;
    }

    @Override
    protected Injector getInjector() {
        return Guice.createInjector(guiceModules);
    }
}
