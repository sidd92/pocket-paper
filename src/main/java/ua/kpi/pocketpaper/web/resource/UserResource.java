package ua.kpi.pocketpaper.web.resource;

import ua.kpi.pocketpaper.persistence.entity.User;
import ua.kpi.pocketpaper.service.UserService;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/rest/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private UserService userService;

    @Inject
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @GET
    public User getUser(@Context SecurityContext securityContext) {
        return (User) securityContext.getUserPrincipal();
    }

    @POST
    @PermitAll
    public Response signUp(@NotNull @FormParam("username") String username,
                           @NotNull @FormParam("password") String password) {
        User user = new User();
        user.setUsername(username);
        userService.addUser(user, password);

        return Response.ok().build();
    }
}
