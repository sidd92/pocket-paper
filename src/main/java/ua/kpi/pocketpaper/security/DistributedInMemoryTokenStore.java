package ua.kpi.pocketpaper.security;

import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.pocketpaper.persistence.entity.User;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;

@Singleton
public class DistributedInMemoryTokenStore implements TokenStore {

    private static final Logger logger = LoggerFactory.getLogger(TokenStore.class);

    private Map<String, User> tokenToUser;
    private Map<User, String> userToToken;

    @Inject
    public DistributedInMemoryTokenStore(HazelcastInstance hazelcastInstance) {
        tokenToUser = hazelcastInstance.getMap("tokenToUser");
        userToToken = hazelcastInstance.getMap("userToToken");
    }

    @Override
    public void saveToken(String token, User user) {
        tokenToUser.put(token, user);
        userToToken.put(user, token);

        logger.info("Saved token for user [{}]", user.getUsername());
    }

    @Override
    public User getUserByToken(String token) {
        return tokenToUser.get(token);
    }

    @Override
    public String getTokenByUser(User user) {
        return userToToken.get(user);
    }
}
