package ua.kpi.pocketpaper.security;

import ua.kpi.pocketpaper.persistence.entity.User;

import javax.inject.Singleton;
import java.util.UUID;

@Singleton
public class SimpleTokenProvider implements TokenProvider {

    @Override
    public String provide(User user) {
        return UUID.randomUUID().toString();
    }
}
