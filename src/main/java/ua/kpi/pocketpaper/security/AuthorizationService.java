package ua.kpi.pocketpaper.security;

import ua.kpi.pocketpaper.persistence.entity.User;

public interface AuthorizationService {

    String authorize(String username, String password) throws SecurityException;

    User authorize(String token) throws SecurityException;

    boolean checkPassword(User user, String password);
}
