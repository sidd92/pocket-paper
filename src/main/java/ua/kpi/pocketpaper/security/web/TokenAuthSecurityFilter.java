package ua.kpi.pocketpaper.security.web;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.pocketpaper.persistence.entity.User;
import ua.kpi.pocketpaper.security.AuthorizationService;
import ua.kpi.pocketpaper.security.SecurityException;

import javax.inject.Inject;

public class TokenAuthSecurityFilter implements ResourceFilter, ContainerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthSecurityFilter.class);

    private static final String AUTH_TYPE = "Bearer";

    private AuthorizationService authorizationService;

    @Inject
    public TokenAuthSecurityFilter(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Override
    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    @Override
    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

    @Override
    public ContainerRequest filter(ContainerRequest request) {
        String token = parseToken(request);
        User user = authorizationService.authorize(token);
        request.setSecurityContext(new SecurityContextImpl(user));

        logger.debug("Authorized request [{}] for user [{}]", request.getAbsolutePath(), user.getUsername());
        return request;
    }

    private String parseToken(ContainerRequest request) {
        String authHeaderValue = request.getHeaderValue("Authorization");
        if (authHeaderValue != null) {
            String[] splitValue = authHeaderValue.split(" ");
            if (splitValue.length == 2) {
                if (splitValue[0].equals(AUTH_TYPE)) {
                    return splitValue[1];
                }
            }
        }

        logger.error("Invalid authorization header");
        throw new SecurityException("Invalid authorization header");
    }
}
