package ua.kpi.pocketpaper.service;

import ua.kpi.pocketpaper.persistence.entity.Article;

public interface ArticleExtractionService {

    Article createArticle(String url);

    String extractTitle(String url);

    String extractArticle(String url);
}
