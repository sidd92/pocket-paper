package ua.kpi.pocketpaper.service;

import ua.kpi.pocketpaper.persistence.entity.Article;
import ua.kpi.pocketpaper.persistence.entity.User;

import java.util.List;

public interface UserService {

    User getUser(Object userId);

    Article getArticle(Object articleId);

    List<Article> getArticles(Object userId);

    void saveArticle(User user, String articleUrl);

    void addUser(User user, String password);
}
