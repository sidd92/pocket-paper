package ua.kpi.pocketpaper;

import com.google.inject.Module;
import com.google.inject.servlet.GuiceFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.pocketpaper.inject.*;
import ua.kpi.pocketpaper.web.GuiceServletConfig;

import javax.servlet.DispatcherType;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.EnumSet;
import java.util.Properties;

public class Bootstrap {

    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

    private String webHost;
    private int webPort;

    private ApplicationStartedEventListener appStartedListener;

    public Bootstrap(String webHost, int webPort) {
        this.webHost = webHost;
        this.webPort = webPort;
    }

    public Bootstrap(String webHost, int webPort, ApplicationStartedEventListener appStartedListener) {
        this.webHost = webHost;
        this.webPort = webPort;
        this.appStartedListener = appStartedListener;
    }

    public void startApplication(Module... guiceModules) {
        Server server = new Server(new InetSocketAddress(webHost, webPort));

        ServletContextHandler servletContextHandler = new ServletContextHandler(server, "/",
                ServletContextHandler.NO_SESSIONS);
        servletContextHandler.addEventListener(new GuiceServletConfig(guiceModules));
        servletContextHandler.addFilter(new FilterHolder(GuiceFilter.class), "/rest/*", EnumSet.of(DispatcherType.REQUEST));
        servletContextHandler.addServlet(DefaultServlet.class, "/*");
        servletContextHandler.setResourceBase(Bootstrap.class.getClassLoader().getResource("web").getFile());

        try {
            logger.info("Starting Jetty");
            server.start();
            if (appStartedListener != null) {
                appStartedListener.started();
            }
            server.join();
        } catch (Exception e) {
            logger.error("Jetty has failed with unexpected error", e);
            throw new RuntimeException("Jetty has failed with unexpected error");
        }
    }

    public static void main(String[] args) {
        Integer port = null;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        }
        Properties webProperties = getWebProperties();
        new Bootstrap(webProperties.getProperty("web.host"),
                port != null ? port : Integer.parseInt(webProperties.getProperty("web.port")))
                .startApplication(productionModules());
    }

    private static Properties getWebProperties() {
        Properties webProperties = new Properties();
        try {
            webProperties.load(new FileReader("web.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return webProperties;
    }

    public static Module[] productionModules() {
        return new Module[] {
                new DaoModule(),
                new ServiceModule(),
                new SecurityModule(),
                new WebModule(),
                new HazelcastModule()
        };
    }

    public static interface ApplicationStartedEventListener {
        void started();
    }
}
