package ua.kpi.pocketpaper.inject;

import com.google.inject.AbstractModule;
import ua.kpi.pocketpaper.service.ArticleExtractionService;
import ua.kpi.pocketpaper.service.ArticleExtractionServiceImpl;
import ua.kpi.pocketpaper.service.UserService;
import ua.kpi.pocketpaper.service.UserServiceImpl;

public class ServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(UserService.class).to(UserServiceImpl.class);
        bind(ArticleExtractionService.class).to(ArticleExtractionServiceImpl.class);
    }
}
