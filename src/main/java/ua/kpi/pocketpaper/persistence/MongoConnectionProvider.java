package ua.kpi.pocketpaper.persistence;

import com.google.inject.Provider;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Igor
 */
@Singleton
public class MongoConnectionProvider implements Provider<Datastore> {

    private String dbName;
    private Set<String> hosts;
    private Datastore ds;

    @Inject
    public MongoConnectionProvider(@Named("dbName") String dbName, @Named("hosts") String hosts) {
        this.dbName = dbName;
        this.hosts = new HashSet<>(Arrays.asList(hosts.split(",")));
    }

    @Override
    public synchronized Datastore get() {
        if (ds == null) {
            ArrayList<ServerAddress> addrs = new ArrayList<ServerAddress>();
            for (String s : hosts) {
                try {
                    addrs.add(new ServerAddress(s));
                } catch (UnknownHostException e) {
                    throw new RuntimeException(e);
                }
            }
            MongoClient mongo = new MongoClient(addrs);
            ds = new Morphia().createDatastore(mongo, dbName);
        }
        return ds;
    }
}
