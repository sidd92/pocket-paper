package ua.kpi.pocketpaper.util;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.jayway.restassured.RestAssured;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockito.Mockito;
import ua.kpi.pocketpaper.Bootstrap;
import ua.kpi.pocketpaper.persistence.dao.ArticleDao;
import ua.kpi.pocketpaper.persistence.dao.UserDao;
import ua.kpi.pocketpaper.web.UserFunctionalityTest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.mockito.Mockito.mock;

public class AbstractFunctionalTest {

    private static Bootstrap bootstrap;

    private static Runnable runApplicationJob = new Runnable() {

        @Override
        public void run() {
            Module[] modules = Bootstrap.productionModules();
            modules[0] = new AbstractModule() {
                @Provides
                UserDao provideUserDao() {
                    return userDao;
                }
                @Provides
                ArticleDao provideArticleDao() {
                    return articleDao;
                }
                @Override
                protected void configure() {
                }
            };
            bootstrap.startApplication(modules);
        }
    };

    private static Bootstrap.ApplicationStartedEventListener appStartedEventListener =
            new Bootstrap.ApplicationStartedEventListener() {

                @Override
                public void started() {
                    synchronized (UserFunctionalityTest.class) {
                        UserFunctionalityTest.class.notify();
                    }
                }
            };

    private static final ExecutorService applicationExecutor = Executors.newSingleThreadExecutor();

    protected static final ArticleDao articleDao = mock(ArticleDao.class);
    protected static final UserDao userDao = mock(UserDao.class);

    @BeforeClass
    public static void setup() {
        bootstrap = new Bootstrap("127.0.0.1", 8000, appStartedEventListener);
        RestAssured.baseURI = "http://127.0.0.1";
        RestAssured.port = 8000;

        synchronized (UserFunctionalityTest.class) {
            applicationExecutor.execute(runApplicationJob);

            try {
                // waiting until application starts
                UserFunctionalityTest.class.wait(10000);
            } catch (InterruptedException e) { }
        }
    }

    @AfterClass
    public static void cleanup() {
        applicationExecutor.shutdown();
    }

    @Before
    public void resetMocks() {
        Mockito.reset(userDao);
        Mockito.reset(articleDao);
    }
}
